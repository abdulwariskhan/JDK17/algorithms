package waris.analyst.stringalogrithm.matchummatch;

import java.util.ArrayList;
import java.util.List;
//java 17
public class FindMatchedAndUnMatchedStringJDK17
{
    public static void main( String[] args )
    {
        List<String> stringList = new ArrayList<>();
        stringList.add("Abdul");
        stringList.add("Waris");
        stringList.add("Khan");

        List<String> row = new ArrayList<>();
        row.add("Demo");
        row.add("JDK");
        row.add("Khan");

        List<String> unmatchedStrings = findUnmatchedStrings(stringList, row);

        System.out.println("Unmatched Strings:");
        for (String unmatchedString : unmatchedStrings) {
            System.out.println(unmatchedString);
        }

        List<String> matchedStrings = findMatchedStrings(stringList, row);

        System.out.println("Matched Strings:");
        for (String matchedString : matchedStrings) {
            System.out.println(matchedString);
        }
    }

    public static List<String> findUnmatchedStrings(List<String> stringList, List<String> row) {
        List<String> unmatchedStrings = new ArrayList<>(row);
        unmatchedStrings.removeAll(stringList);
        return unmatchedStrings;
    }

    public static List<String> findMatchedStrings(List<String> stringList, List<String> row) {
        List<String> matchedStrings = new ArrayList<>(row);
        matchedStrings.retainAll(stringList);
        return matchedStrings;
    }
}
